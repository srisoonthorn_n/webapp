import React from 'react';
import { Input, List, Avatar, Skeleton, Card } from 'antd';


const { Search } = Input;

class UserPage extends React.Component {
    constructor() {
        super();
        this.state = {
            users: [],
            usersName: "",
        };
    }
    fetchUserData = () => {
        fetch('http://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                this.setState({ users: data })
            })
            .catch(error => console.log(error));

    }
    componentDidMount() {
        this.fetchUserData();
    }
    onSearchFuction = (event) => {
       const value = event.target.value;
       this.setState({usersName : value})
    }
    render() {

        return (
            <div>
                <Card>
                    <center>
                        <Search
                            placeholder="input search text"
                            enterButton="Search"
                            style={{ width: 500 }}

                            onChange={this.onSearchFuction}
                        />
                    </center>
                </Card>
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={
                        this.state.usersName == '' ?
                            this.state.users
                            :
                            this.state.users.filter(m => m.name.match(this.state.usersName))
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/users/" + item.id + '/todo'}>Todo</a>, <a href={"/users/" +item.id+"/albumslist"}>Albums</a>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                                    }
                                    title={item.name}
                                    description={item.email}
                                />
                                <div>{item.phone} | {item.website}</div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}
export default UserPage
