import React, { useState, useEffect } from 'react';
import { Descriptions, List, Typography, Select, Button, Skeleton, Pagination, Card} from 'antd';
const { Option } = Select;

const TodoPage = (props) => {
    const [user, setUser] = useState(undefined);
    const [todolist, setTodoList] = useState([]);

    const [selectedTypeIndex, SetSelectedTypeIndex] = useState('All')

    const fetchUserData = () => {
        const userId = props.match.params.user_id
        fetch('http://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
    }
    const fetchTodoData = () => {
        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodoList(data)

            })
    }

    //component DidMount
    useEffect(() => {
        fetchUserData();
        fetchTodoData();
    }, [])

    const DoneTodo = (index) => {
        let todoListTmp = [...todolist];
        todoListTmp[index.id - 1].completed = true;
        setTodoList(todoListTmp);
    }

    const onFilterType = (index) => {
        let keepType = index
        SetSelectedTypeIndex(keepType)
    }

    return (
        <div>
            <Card>
                {user != undefined &&
                    <Descriptions title={"User : " + user.name}>
                        <Descriptions.Item label="User Name">{user.username}</Descriptions.Item>
                        <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                        <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                        <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                        <Descriptions.Item label="Address">{user.address.street}, {user.address.suite}, {user.address.city}</Descriptions.Item>
                    </Descriptions>
                }
            </Card>

            <div>
                <Select defaultValue="All" style={{ width: 120 }} onChange={onFilterType}>
                    <Option value="All">All</Option>
                    <Option value={false}>DOING</Option>
                    <Option value={true}>DONE</Option>
                </Select>
            </div>
            <List
                header={<div><h3>Todo List</h3></div>}
                pagination={{
                    pageSize: 10,
                }}
                footer={<div>Total items : {todolist.length}</div>}
                bordered
                dataSource={
                    selectedTypeIndex == 'All' ?
                        todolist
                        :
                        todolist.filter(m => m.completed == selectedTypeIndex)}
                renderItem={(item) => (
                    <Skeleton avatar title={false} loading={item.loading} active>
                        <List.Item>
                            <Typography.Text mark={!item.completed} delete={item.completed}>
                                {
                                    !item.completed &&
                                    <Button style={{ float: 'right' }} onClick={() => DoneTodo(item)}>Done</Button>
                                }
                                {
                                    item.completed == false ?
                                        "DOING"
                                        :
                                        "DONE"
                                }
                            </Typography.Text> {item.title}
                        </List.Item>
                    </Skeleton>
                )}
            />
        </div >
    )
}
export default TodoPage
