import React, { useEffect, useState } from 'react'
import { Card, List, PageHeader, Descriptions } from 'antd';

const Albumslist = (props) => {
    const [user, setUser] = useState(undefined);
    const [albumlist, setAlbumlist] = useState([]);

    useEffect(() => {
        fetchUserData();
        fetchAlbumsData();
    }, [])

    const fetchAlbumsData = () => {
        const alubumlistId = props.match.params.user_id
        fetch('http://jsonplaceholder.typicode.com/albums?userId=' + alubumlistId)
            .then(response => response.json())
            .then(data => {
                setAlbumlist(data)
            })
    }
    const fetchUserData = () => {
        const userId = props.match.params.user_id
        fetch('http://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
    }
    return (

        <div>
                {user != undefined &&
                    <Descriptions title={"User : " + user.name}>
                    <Descriptions.Item label="User Name">{user.username}</Descriptions.Item>
                    <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                    <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
                    <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                    <Descriptions.Item label="Address">{user.address.street}, {user.address.suite}, {user.address.city}</Descriptions.Item>
                </Descriptions>
                }
            <center>
                <Card>
                    <h1>Albums List</h1>
                    </Card>
                    <Card>
                    <List
                        itemLayout="horizontal"
                        dataSource={albumlist}
                        renderItem={item => (
                            <List.Item>
                                <List.Item.Meta
                                    title={<a href={"/users/" + item.id + "/photolist/" + item.id}>{item.title}</a>}
                                />
                            </List.Item>
                        )}
                    />
                </Card>
            </center>
        </div>
    )
}

export default Albumslist