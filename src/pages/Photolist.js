import React, { useEffect, useState } from 'react'
import { Card, List, Avatar, PageHeader , Spin ,BackTop } from 'antd';
import '../App.css'
const { Meta } = Card;


const Photolist = (props) => {

    const [photolist, setPhotolist] = useState([]);
    const [loading , setLoading ] = useState(false);

    useEffect(() => {
        fetchPhotoData();
    }, [])

    const fetchPhotoData = () => {
        const alubumId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + alubumId)
            .then(response => response.json())
            .then(data => {
                setLoading(true)
                setTimeout(()=> {
                    setLoading (false)
                },5500) 
                setPhotolist(data)
            })
    }

    return (
        <div>
            <Spin size="large"
            className="spinner"
            tip="Loading..."
            spinning={loading}>
            <Card style={{ background: '#9ab7d9'}}>
                <PageHeader
                    ghost={false}
                    title=" "
                    onBack={() => window.history.back()}
                >
                </PageHeader>
                <List
                    grid={{
                        gutter: 5,
                        xs: 1,
                        sm: 2,
                        md: 4,
                        lg: 4,
                        xl: 6,
                        xxl: 3,
                    }}
                    dataSource={photolist}
                    renderItem={item => (
                        <List.Item>
                            <center>
                                <Card
                                    cover={
                                        <img
                                            style={{ width: 250, marginTop: 20 }}
                                            src={item.url}
                                        />
                                    }>
                                </Card>
                                <Card style={{ width: 500, height: 80 }}>
                                    <Meta
                                        avatar={
                                            <Avatar src={item.thumbnailUrl} />
                                        }
                                        title={item.title}
                                        description={item.id}
                                    />
                                </Card>
                            </center>
                        </List.Item>
                    )}
                />
            </Card>
            <BackTop />
    <strong style={{ color: 'rgba(64, 64, 64, 0.6)' }}></strong>
            </Spin>
        </div>
    )
}
export default Photolist