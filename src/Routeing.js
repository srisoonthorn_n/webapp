import React from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import UserPage from './pages/UserPage';
import TodoPage from './pages/Todopage';
import Menu from './component/Menu';
import Albumslist from './pages/Albumslist';
import Photolist from './pages/Photolist';

const Routeing = () => {
    return (
        <BrowserRouter>
            <Route path="/" component={Menu}/>
            <Route path="/users" component={UserPage} exact={true} />
            <Route path="/users/:user_id/todo" component={TodoPage} />
            <Route path="/users/:user_id/photolist/:user_id" component={Photolist} />
            <Route path="/users/:user_id/albumslist" component={Albumslist} />
        </BrowserRouter>
    )
}
export default Routeing


